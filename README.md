# zj-gencode

#### 项目介绍
Java代码简易生成工具，根据excel生成sql、Java类、dao、mybaties模版等。
项目依赖[JDK8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
excel解析依赖[poi](https://poi.apache.org/)
模版生成依赖[velocity](http://velocity.apache.org/)。

#### 软件架构
本项目主要的流程为：
1. 解析excel提取数据
2. 数据验证
3. 转换成模版对象
4. 根据模版对象生成文件


#### 安装教程
目前直接pull代码运行Client即可，如有必要，后期考虑改为web项目。

#### 使用说明
1. 修改resource中config文件配置
2. 修改excel定义自己的模型
3. 运行Client即可
4. 拷贝生成文件到项目中，如果你也使用mybaties并使用了IdEnum，将dependency目录文件一起拷贝到对应位置

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)