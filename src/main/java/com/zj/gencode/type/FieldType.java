package com.zj.gencode.type;

import com.zj.util.utils.StringUtil;

/**
 * @author Mr. xi.yang<br/>
 * @version V1.0 <br/>
 * @description: 数据类型 <br/>
 * @date 2017-09-27 上午 10:21 <br/>
 */
public enum FieldType {
    /**
     * 继承了{IdEnum}的enum类型
     */
    ID_ENUM,
    /**
     * java类,普通pojo
     */
    JAVA_BEAN,
    INTEGER,
    BOOLEAN,
    LONG,
    FLOAT,
    DOUBLE,
    STRING,
    DATE,
    ;

    public static FieldType getFieldTypeByName(String name){
        if(StringUtil.isBlank(name)){
            return null;
        }
        for (FieldType temp : FieldType.values()) {
            if(temp.toString().equals(name)){
                return temp;
            }
        }
        return null;
    }
}
